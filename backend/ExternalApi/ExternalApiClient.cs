using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace GrpcDemo.Server.ExternalApi
{
    public class ExternalApiClient
    {
        public async Task<NewChuckNorrisJoke> GetRandomJoke()
        {
            using var client = new HttpClient();

            var json = await client.GetFromJsonAsync<JokeResponse>("https://api.chucknorris.io/jokes/random");
            return new NewChuckNorrisJoke
            {
                Id = json.id,
                Url = json.url,
                IconUrl = json.icon_url,
                Value = json.value,
            };
        }
    }
}