using System.Threading.Tasks;
using Grpc.Core;
using GrpcDemo.Server.DataAccess;
using GrpcDemo.Server.ExternalApi;
using Microsoft.Extensions.Logging;

namespace GrpcDemo.Server.Api
{
    public class ChuckNorrisJokesService : ChuckNorrisJokes.ChuckNorrisJokesBase
    {
        private readonly ILogger<ChuckNorrisJokesService> _logger;
        private readonly ExternalApiClient _client;
        private readonly Repository _repository;

        public ChuckNorrisJokesService(ILogger<ChuckNorrisJokesService> logger, ExternalApiClient client, Repository repository)
        {
            _logger = logger;
            _client = client;
            _repository = repository;
        }

        public override async Task<NewChuckNorrisJoke> GetRandomJoke(EmptyMessage _, ServerCallContext context)
        {
            return await _client.GetRandomJoke();
        }

        public override async Task<EmptyMessage> AddJoke(NewChuckNorrisJoke joke, ServerCallContext context)
        {
            await _repository.Add(joke);
            return new EmptyMessage();
        }

        public override async Task<ChuckNorrisJokeListResponse> ListJokes(EmptyMessage joke, ServerCallContext context)
        {
            return await _repository.GetList();
        }

        public override async Task<EmptyMessage> DeleteJokeById(NewChuckNorrisJokePrimaryId jokeId, ServerCallContext context)
        {
            await _repository.DeleteById(jokeId.Id);
            return new EmptyMessage();
        }
    }
}
