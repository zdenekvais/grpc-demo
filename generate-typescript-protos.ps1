﻿ param(
	[string] $inputFile = ".\backend\Protos\chuck_norris_jokes.proto",
    [string] $outputDir = ".\frontend\src\protos\"
)


$outputDir = Resolve-Path $outputDir

Write-Host "-== GENERATING TS PROTO FILES ==-"
Write-Host "Input proto file: $inputFile"
Write-Host "Output dir: $outputDir"

Remove-Item -Path $outputDir -Force -Recurse
New-Item -Path $outputDir -ItemType Directory


protoc --grpc-web_out=import_style=typescript,mode=grpcwebtext:"$outputDir" "$inputFile"
protoc --js_out=import_style=commonjs,binary:"$outputDir" "$inputFile"