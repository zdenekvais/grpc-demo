import { useState, useEffect } from "react";
import { Container } from "react-bootstrap";

import { Error } from "grpc-web";
import {
  EmptyMessage,
  ChuckNorrisJokeListResponse,
} from "./protos/chuck_norris_jokes_pb";
import { ChuckNorrisJokesClient } from "./protos/Chuck_norris_jokesServiceClientPb";
import JokeList from "./JokeList";
import AddJokeForm from "./AddJokeForm";

function App() {
  const [jokes, setJokes] = useState<ChuckNorrisJokeListResponse | null>(null);

  const client = new ChuckNorrisJokesClient("http://localhost:8080");

  const fetchJokeList = () => {
    let emptyRequest = new EmptyMessage();
    let callback = (_: Error, response: ChuckNorrisJokeListResponse) => {
      setJokes(response);
    };
    client.listJokes(emptyRequest, null, callback);
  };

  useEffect(fetchJokeList, []);

  if (jokes == null) {
    return <div>Loading...</div>;
  }

  return (
    <Container>
      <h1>Chuck Norris jokes</h1>

      <br />

      <AddJokeForm client={client} onChange={fetchJokeList} />
      <JokeList jokes={jokes.getJokesList()} client={client} onChange={fetchJokeList} />

      <br />
    </Container>
  );
}

export default App;
