import * as jspb from 'google-protobuf'



export class EmptyMessage extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): EmptyMessage.AsObject;
  static toObject(includeInstance: boolean, msg: EmptyMessage): EmptyMessage.AsObject;
  static serializeBinaryToWriter(message: EmptyMessage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): EmptyMessage;
  static deserializeBinaryFromReader(message: EmptyMessage, reader: jspb.BinaryReader): EmptyMessage;
}

export namespace EmptyMessage {
  export type AsObject = {
  }
}

export class NewChuckNorrisJokePrimaryId extends jspb.Message {
  getId(): string;
  setId(value: string): NewChuckNorrisJokePrimaryId;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NewChuckNorrisJokePrimaryId.AsObject;
  static toObject(includeInstance: boolean, msg: NewChuckNorrisJokePrimaryId): NewChuckNorrisJokePrimaryId.AsObject;
  static serializeBinaryToWriter(message: NewChuckNorrisJokePrimaryId, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NewChuckNorrisJokePrimaryId;
  static deserializeBinaryFromReader(message: NewChuckNorrisJokePrimaryId, reader: jspb.BinaryReader): NewChuckNorrisJokePrimaryId;
}

export namespace NewChuckNorrisJokePrimaryId {
  export type AsObject = {
    id: string,
  }
}

export class NewChuckNorrisJoke extends jspb.Message {
  getId(): string;
  setId(value: string): NewChuckNorrisJoke;

  getUrl(): string;
  setUrl(value: string): NewChuckNorrisJoke;

  getValue(): string;
  setValue(value: string): NewChuckNorrisJoke;

  getIconUrl(): string;
  setIconUrl(value: string): NewChuckNorrisJoke;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): NewChuckNorrisJoke.AsObject;
  static toObject(includeInstance: boolean, msg: NewChuckNorrisJoke): NewChuckNorrisJoke.AsObject;
  static serializeBinaryToWriter(message: NewChuckNorrisJoke, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): NewChuckNorrisJoke;
  static deserializeBinaryFromReader(message: NewChuckNorrisJoke, reader: jspb.BinaryReader): NewChuckNorrisJoke;
}

export namespace NewChuckNorrisJoke {
  export type AsObject = {
    id: string,
    url: string,
    value: string,
    iconUrl: string,
  }
}

export class ChuckNorrisJoke extends jspb.Message {
  getPrimaryid(): string;
  setPrimaryid(value: string): ChuckNorrisJoke;

  getId(): string;
  setId(value: string): ChuckNorrisJoke;

  getUrl(): string;
  setUrl(value: string): ChuckNorrisJoke;

  getValue(): string;
  setValue(value: string): ChuckNorrisJoke;

  getIconUrl(): string;
  setIconUrl(value: string): ChuckNorrisJoke;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChuckNorrisJoke.AsObject;
  static toObject(includeInstance: boolean, msg: ChuckNorrisJoke): ChuckNorrisJoke.AsObject;
  static serializeBinaryToWriter(message: ChuckNorrisJoke, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChuckNorrisJoke;
  static deserializeBinaryFromReader(message: ChuckNorrisJoke, reader: jspb.BinaryReader): ChuckNorrisJoke;
}

export namespace ChuckNorrisJoke {
  export type AsObject = {
    primaryid: string,
    id: string,
    url: string,
    value: string,
    iconUrl: string,
  }
}

export class ChuckNorrisJokeListResponse extends jspb.Message {
  getJokesList(): Array<ChuckNorrisJoke>;
  setJokesList(value: Array<ChuckNorrisJoke>): ChuckNorrisJokeListResponse;
  clearJokesList(): ChuckNorrisJokeListResponse;
  addJokes(value?: ChuckNorrisJoke, index?: number): ChuckNorrisJoke;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChuckNorrisJokeListResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ChuckNorrisJokeListResponse): ChuckNorrisJokeListResponse.AsObject;
  static serializeBinaryToWriter(message: ChuckNorrisJokeListResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChuckNorrisJokeListResponse;
  static deserializeBinaryFromReader(message: ChuckNorrisJokeListResponse, reader: jspb.BinaryReader): ChuckNorrisJokeListResponse;
}

export namespace ChuckNorrisJokeListResponse {
  export type AsObject = {
    jokesList: Array<ChuckNorrisJoke.AsObject>,
  }
}

