/**
 * @fileoverview gRPC-Web generated client stub for ChuckNorrisJokes
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck


import * as grpcWeb from 'grpc-web';

import * as backend_Protos_chuck_norris_jokes_pb from '../../backend/Protos/chuck_norris_jokes_pb';


export class ChuckNorrisJokesClient {
  client_: grpcWeb.AbstractClientBase;
  hostname_: string;
  credentials_: null | { [index: string]: string; };
  options_: null | { [index: string]: any; };

  constructor (hostname: string,
               credentials?: null | { [index: string]: string; },
               options?: null | { [index: string]: any; }) {
    if (!options) options = {};
    if (!credentials) credentials = {};
    options['format'] = 'text';

    this.client_ = new grpcWeb.GrpcWebClientBase(options);
    this.hostname_ = hostname;
    this.credentials_ = credentials;
    this.options_ = options;
  }

  methodInfoGetRandomJoke = new grpcWeb.AbstractClientBase.MethodInfo(
    backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke,
    (request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage) => {
      return request.serializeBinary();
    },
    backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke.deserializeBinary
  );

  getRandomJoke(
    request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    metadata: grpcWeb.Metadata | null): Promise<backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke>;

  getRandomJoke(
    request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke) => void): grpcWeb.ClientReadableStream<backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke>;

  getRandomJoke(
    request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChuckNorrisJokes.ChuckNorrisJokes/GetRandomJoke',
        request,
        metadata || {},
        this.methodInfoGetRandomJoke,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChuckNorrisJokes.ChuckNorrisJokes/GetRandomJoke',
    request,
    metadata || {},
    this.methodInfoGetRandomJoke);
  }

  methodInfoListJokes = new grpcWeb.AbstractClientBase.MethodInfo(
    backend_Protos_chuck_norris_jokes_pb.ChuckNorrisJokeListResponse,
    (request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage) => {
      return request.serializeBinary();
    },
    backend_Protos_chuck_norris_jokes_pb.ChuckNorrisJokeListResponse.deserializeBinary
  );

  listJokes(
    request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    metadata: grpcWeb.Metadata | null): Promise<backend_Protos_chuck_norris_jokes_pb.ChuckNorrisJokeListResponse>;

  listJokes(
    request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.ChuckNorrisJokeListResponse) => void): grpcWeb.ClientReadableStream<backend_Protos_chuck_norris_jokes_pb.ChuckNorrisJokeListResponse>;

  listJokes(
    request: backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.ChuckNorrisJokeListResponse) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChuckNorrisJokes.ChuckNorrisJokes/ListJokes',
        request,
        metadata || {},
        this.methodInfoListJokes,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChuckNorrisJokes.ChuckNorrisJokes/ListJokes',
    request,
    metadata || {},
    this.methodInfoListJokes);
  }

  methodInfoAddJoke = new grpcWeb.AbstractClientBase.MethodInfo(
    backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    (request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke) => {
      return request.serializeBinary();
    },
    backend_Protos_chuck_norris_jokes_pb.EmptyMessage.deserializeBinary
  );

  addJoke(
    request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke,
    metadata: grpcWeb.Metadata | null): Promise<backend_Protos_chuck_norris_jokes_pb.EmptyMessage>;

  addJoke(
    request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.EmptyMessage) => void): grpcWeb.ClientReadableStream<backend_Protos_chuck_norris_jokes_pb.EmptyMessage>;

  addJoke(
    request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJoke,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.EmptyMessage) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChuckNorrisJokes.ChuckNorrisJokes/AddJoke',
        request,
        metadata || {},
        this.methodInfoAddJoke,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChuckNorrisJokes.ChuckNorrisJokes/AddJoke',
    request,
    metadata || {},
    this.methodInfoAddJoke);
  }

  methodInfoDeleteJokeById = new grpcWeb.AbstractClientBase.MethodInfo(
    backend_Protos_chuck_norris_jokes_pb.EmptyMessage,
    (request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJokePrimaryId) => {
      return request.serializeBinary();
    },
    backend_Protos_chuck_norris_jokes_pb.EmptyMessage.deserializeBinary
  );

  deleteJokeById(
    request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJokePrimaryId,
    metadata: grpcWeb.Metadata | null): Promise<backend_Protos_chuck_norris_jokes_pb.EmptyMessage>;

  deleteJokeById(
    request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJokePrimaryId,
    metadata: grpcWeb.Metadata | null,
    callback: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.EmptyMessage) => void): grpcWeb.ClientReadableStream<backend_Protos_chuck_norris_jokes_pb.EmptyMessage>;

  deleteJokeById(
    request: backend_Protos_chuck_norris_jokes_pb.NewChuckNorrisJokePrimaryId,
    metadata: grpcWeb.Metadata | null,
    callback?: (err: grpcWeb.Error,
               response: backend_Protos_chuck_norris_jokes_pb.EmptyMessage) => void) {
    if (callback !== undefined) {
      return this.client_.rpcCall(
        this.hostname_ +
          '/ChuckNorrisJokes.ChuckNorrisJokes/DeleteJokeById',
        request,
        metadata || {},
        this.methodInfoDeleteJokeById,
        callback);
    }
    return this.client_.unaryCall(
    this.hostname_ +
      '/ChuckNorrisJokes.ChuckNorrisJokes/DeleteJokeById',
    request,
    metadata || {},
    this.methodInfoDeleteJokeById);
  }

}

