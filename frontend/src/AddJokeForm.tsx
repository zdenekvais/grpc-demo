import { useState } from "react";
import { Form, Col, Row, Button } from "react-bootstrap";

import { Error } from "grpc-web";
import {
  EmptyMessage,
  NewChuckNorrisJoke,
} from "./protos/chuck_norris_jokes_pb";
import { ChuckNorrisJokesClient } from "./protos/Chuck_norris_jokesServiceClientPb";

interface AddJokeFormProps {
  client: ChuckNorrisJokesClient;
  onChange: () => void;
}

function copyJoke(joke: NewChuckNorrisJoke): NewChuckNorrisJoke {
    const newJoke = new NewChuckNorrisJoke();
    newJoke.setIconUrl(joke.getIconUrl());
    newJoke.setId(joke.getId());
    newJoke.setUrl(joke.getUrl());
    newJoke.setValue(joke.getValue());
    return newJoke;
}

export default function AddJokeForm({ client, onChange }: AddJokeFormProps) {
  const [isFetchingRandomJoke, setIsFetchingRandomJoke] = useState(false);
  const [joke, setJoke] = useState<NewChuckNorrisJoke>(
    new NewChuckNorrisJoke()
  );

  const fetchRandomJoke = () => {
    let emptyRequest = new EmptyMessage();
    setIsFetchingRandomJoke(true);
    let callback = (err: Error, response: NewChuckNorrisJoke) => {
      setJoke(response);
      setIsFetchingRandomJoke(false);
    };
    client.getRandomJoke(emptyRequest, null, callback);
  };

  const onTextChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newJoke = copyJoke(joke);
    newJoke.setValue(e.target.value);
    setJoke(newJoke);
  };

  const onUrlChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newJoke = copyJoke(joke);
    newJoke.setUrl(e.target.value);
    setJoke(newJoke);
  };

  const onIconUrlChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newJoke = copyJoke(joke);
    newJoke.setIconUrl(e.target.value);
    setJoke(newJoke);
  };

  const onIdChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    const newJoke = copyJoke(joke);
    newJoke.setId(e.target.value);
    setJoke(newJoke);
  };

  const onAddClicked = () => client.addJoke(joke, null, onChange);

  const fetchRandomJokeButtonText = isFetchingRandomJoke
    ? "Fetching..."
    : "Fetch random joke";

  return (
    <div>
      <Form>
        <Form.Group as={Row}>
          <Form.Label column sm="2">
            Id
          </Form.Label>
          <Col sm="10">
            <Form.Control type="text" placeholder="Id" value={joke.getId()} onChange={onIdChange} />
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column sm="2">Url</Form.Label>
          <Col sm="10">
            <Form.Control type="text" placeholder="Url" value={joke.getUrl()} onChange={onUrlChange} />
          </Col>
        </Form.Group>
        <Form.Group as={Row}>
          <Form.Label column sm="2">Icon Url</Form.Label>
          <Col sm="10">
            <Form.Control
              type="text"
              placeholder="Icon Url"
              value={joke.getIconUrl()}
              onChange={onIconUrlChange}
            />
          </Col>
        </Form.Group>
        <Form.Group>
          <Form.Control
            onChange={onTextChange}
            value={joke.getValue()}
            as="textarea"
            rows={3}
          />
        </Form.Group>
        <Form.Group>
          <Button onClick={onAddClicked} variant="success" size="sm">Add</Button> &nbsp;
          <Button onClick={fetchRandomJoke} variant="primary" size="sm">{fetchRandomJokeButtonText}</Button>
        </Form.Group>
      </Form>
    </div>
  );
}
