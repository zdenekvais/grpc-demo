import { ChuckNorrisJoke } from "./protos/chuck_norris_jokes_pb";
import { ListGroup, Button } from "react-bootstrap";

interface JokeProps {
  joke: ChuckNorrisJoke;
  deleteJoke: () => void;
}

const iconStyle = { width: 30, height: 30, marginRight: 5};

export default function Joke({ joke, deleteJoke }: JokeProps) {
  return (
    <ListGroup.Item id={`joke-${joke.getId()}`}>
      <Button variant="danger" size="sm" onClick={deleteJoke}>Delete</Button> &nbsp;
      <img src={joke.getIconUrl()} style={iconStyle} alt="joke-icon" />
      <a href={joke.getUrl()}>URL</a> &nbsp;
      <em>{joke.getValue()}</em>
    </ListGroup.Item>
  );
}
