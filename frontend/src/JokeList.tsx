import { ListGroup } from "react-bootstrap";

import { ChuckNorrisJoke, NewChuckNorrisJokePrimaryId } from "./protos/chuck_norris_jokes_pb";
import { ChuckNorrisJokesClient } from "./protos/Chuck_norris_jokesServiceClientPb";

import Joke from "./Joke";

interface JokeListProps {
    jokes: ChuckNorrisJoke[];
    client: ChuckNorrisJokesClient;
    onChange: () => void;
}

export default function JokeList({ jokes, client, onChange }: JokeListProps) { 
  const jokeElements = jokes.map((joke: ChuckNorrisJoke) => {
    const deleteJoke = () => {
      const jokeIdRequest = new NewChuckNorrisJokePrimaryId();
      jokeIdRequest.setId(joke.getPrimaryid());
      client.deleteJokeById(jokeIdRequest, null, onChange);
    }

    return <Joke key={joke.getPrimaryid()} joke={joke} deleteJoke={deleteJoke} />
  });
  return <ListGroup id="jokes">{jokeElements}</ListGroup>;
}