## Database

``` bash
$ cd backend
$ sqlite3 jokes.db
> CREATE TABLE jokes (
    primaryId INTEGER PRIMARY KEY AUTOINCREMENT,
    id TEXT, 
    url TEXT, 
    value TEXT, 
    iconUrl TEXT
);
```
## Starting the app
- frontend `cd frontend && npm start`
- backend `cd backend && dotnet run`
- envoy proxy `./run-envoy.sh`

## Typescript client
- grpc web typescript client generation - https://github.com/grpc/grpc-web/releases
- envoy proxy with grpc web http filter - https://blog.envoyproxy.io/envoy-and-grpc-web-a-fresh-new-alternative-to-rest-6504ce7eb880

## Problems (linux)
- distributions are missing a single dll resulting in omnisharp crashing - https://github.com/OmniSharp/omnisharp-vscode/issues/4360
- expired certificates broke