#!/bin/bash

PROTOC_GEN_TS_PATH="./frontend/node_modules/.bin/protoc-gen-ts"
OUT_DIR="./frontend/src/protos"
PROTOS_PATH="./backend/Protos"
PROTOS=${PROTOS_PATH}/chuck_norris_jokes.proto

rm -rf $OUT_DIR
mkdir $OUT_DIR

PATH=$PATH:$pwd protoc -I=$PROTOS_PATH $PROTOS \
    --js_out=import_style=commonjs,binary:$OUT_DIR \
    --grpc-web_out=import_style=typescript,mode=grpcwebtext:$OUT_DIR